
from os import renames
from statistics import mean
from turtle import color
import numpy as np
import pandas as pd
import seaborn as sns   
import matplotlib.pyplot as plt
from sklearn import preprocessing,svm 
from sklearn.model_selection import train_test_split    
from sklearn.linear_model import LinearRegression

# read csv file from archive folder

def read_csv(filename):
    """
    Function to read a csv file and return a list of dataframes
    """
    f = open(filename)
    reader = csv.reader(f)

# load data from bottle file

data_df = pd.read_csv('../../../data/train.csv')
data_df['target'] = data_df['target'].astype('category')
data_df['label'] =

df=pd.read_csv(r'C:\Users\rajath.jain\OneDrive - Accenture\Desktop\archive\bottle.csv')
df_binary = df[['Salary','T_degc']]

df_binary.columns=['Sal','Temp']

#display first 6 rows of df_binary

print(df_binary.head())
print(df_binary.shape)
print(df_binary.dtypes)
print(df_binary.index)

sns.lmplot(x='Sal', y='Temp',data=df_binary,order=2,ci=None)

#Eliminate outliers NaN or missing input numbers
df_binary.fillna(method='ffill',inplace=True)

X=np.array(df_binary['Sal']).reshape(-1,1)
y=np.array(df_binary['Temp']).reshape(-1,1)

df_binary.dropna(inplace=True)

X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.25)

regr=LinearRegression()
regr.fit(X_train,y_train)

print(regr.score(X_test,y_test))

y_pred=regr.predict(X_test)
plt.scatter(X_test,y_test,color='b')
plt.plt(X_test,y_pred,color='k')
plt.show()

from sklearn.metrics import mean_absolute_error,mean_squared_error

mae=mean_absolute_error(y_true=y_test,y_pred=y_pred)
mse=mean_squared_error(y_true=y_test,y_pred=y_pred)
rmse=mean_squared_error(y_true=y_test,y_pred=y_pred,squared=False)

print("Mean Squared Error:",mae)
print("MSE:",mse)
print("RMSE:",rmse)
